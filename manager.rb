require 'byebug'
require_relative 'employee'

class Manager < Employee
  attr_reader :assigned_employees

  def initialize(name, boss = nil, salary, title)
    super(name, boss, salary, title)
    @assigned_employees = []
  end

  def bonus(multiplier)
    assigned_employees.each_with_object([]) do |employee, employee_salaries|
      employee_salaries << employee.salary
    end.inject(:+) * multiplier
  end

  def add_subordinate(*employees)
    # debugger
    employees.each do |employee|
      unless assigned_employees.include?(employee)
        employee.boss = self
        assigned_employees << employee
      end
    end
  end
end
