module Cursor

  KEYMAP = {
    "w" => :up,
    "a" => :left,
    "s" => :down,
    "d" => :right,
    "\r" => :return,
    "\e" => :escape,
    "\e[A" => :up,
    "\e[B" => :down,
    "\e[C" => :right,
    "\e[D" => :left,
    "\177" => :backspace,
    "\u0003" => :ctrl_c,
  }

  MOVES = {
    left: [0, -1],
    right: [0, 1],
    up: [-1, 0],
    down: [1, 0]
  }


  def read_keystroke
    STDIN.echo = false
    STDIN.raw!

    input = STDIN.getc.chr
    if input == "\e" then
      input << STDIN.read_nonblock(3) rescue nil
      input << STDIN.read_nonblock(2) rescue nil
    end
  ensure
    STDIN.echo = true
    STDIN.cooked!

    return input
  end

  def get_input
    begin
      key = KEYMAP[read_keystroke]
      handle_key(key)
    rescue
      retry
    end
  end

  def handle_key(key)
    case key
    when :ctrl_c
      exit 0
    when :return
      @cursor
    when :left, :right, :up, :down
      update_board(MOVES[key])
      nil
    end
  end

  def update_board(dif)
    new_pos = [@cursor[0] + dif[0], @cursor[1] + dif[1]]

    if on_board?(new_pos)
      @cursor = new_pos
    else
      raise MoveError
    end
  end

  def on_board?(pos)
    row, col = pos
    (0..9).cover?(row) && (0..9).cover?(col)
  end
end
