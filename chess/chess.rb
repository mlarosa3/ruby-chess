require_relative 'piece'
require_relative 'board'
require_relative 'cursor'
require_relative 'display'
require_relative 'human_player'

class Chess

  attr_reader :board, :display, :players, :current_player

  def initialize(board, display, players, current_player = :white)
    @current_player = current_player
    @board = board
    @display = display
    @players = players
  end

  def play
    until won?
      players[0].make_move
      swap_turn!
    end
  end

  private

  def won?
    false
  end

  def notify_players
  end

  def swap_turn!
    puts "We got to swap turn"
  end
end

if __FILE__ == $0
  board = Board.new
  display = Display.new(board)
  players = [HumanPlayer.new(display), HumanPlayer.new(display)]
  Chess.new(board, display, players).play
end
