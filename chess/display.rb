require 'colorize'
require 'io/console'
require_relative 'cursor'

class Display
  include Cursor

  attr_reader :cursor, :board

  def initialize(board)
    @board = board
    # @game = game
    @cursor = [0, 0]
    @selected = false
  end

  def build_grid
    board.grid.map.with_index do |row, i|
      build_row(row, i)
    end
  end

  def build_row(row, i)
    row.map.with_index do |sq, j|
      color = determine_color(i, j)
      sq.to_s.colorize(color)
    end
  end

  def determine_color(i, j)
    if [i, j] == cursor
      bg = :light_blue
    elsif (i + j).odd?
      bg = :light_red
    else
      bg = :light_black
    end

    { background: bg, color: :white }
  end

  def render
    system('clear')

    build_grid.each do |row|
      puts row.join
    end
  end
end
