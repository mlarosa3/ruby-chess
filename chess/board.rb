require_relative 'null_piece'

class Board

  attr_reader :grid

  def initialize
    @grid = make_starting_grid
  end

  def move_piece(from_pos, to_pos)
    begin
      unless grid[from_pos].is_a?(Piece) && valid_moves?(to_pos)
        raise MoveError.new("Invalid Move")
      end
      move_piece!(from_pos, to_pos)
    rescue MoveError => e
      e.message
      retry
    end
  end

  def move_piece!(from_pos, to_pos)

  end

  def [](pos)
    row, col = pos

    grid[col][row]
  end

  def []=(new_pos, piece)
    row, col = new_pos

    grid[col][row] = piece
  end

  def dup

  end

  def checkmate?

  end

  def valid_moves?(pos)
    row, col = pos
    (0..9).cover?(row) &&
      (0..9).cover?(col) &&
      grid[pos].nil?
  end

  protected

  def find_kind(color)
  end

  def make_starting_grid
    Array.new(10) { Array.new(10) { NullPiece.new } }
  end
end
