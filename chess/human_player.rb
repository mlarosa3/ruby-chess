class HumanPlayer

  attr_reader :display

  def initialize(display)
    @display = display
  end

  def make_move
    result = nil
    until result
      display.render
      result = display.get_input
    end
    result
  end
end
