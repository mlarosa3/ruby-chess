require_relative 'manager'

ned = Manager.new("Ned", nil, 1_000_000, "Founder")
darren = Manager.new("Darren", ned, 78_000, "TA Manager")
david = Employee.new("David", darren, 10_000, "TA")
shawna = Employee.new("Shawna", darren, 12_000, "TA")
darren.add_subordinate(shawna, david)
ned.add_subordinate(darren, shawna, david)

p ned.bonus(5) # => 500_000
p darren.bonus(4) # => 88_000
p david.bonus(3) # => 30_000
