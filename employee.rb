class Employee
  attr_reader :salary, :name, :title
  attr_accessor :boss

  def initialize(name, boss = nil, salary, title)
    @name = name
    @salary = salary
    @title = title
    @boss = boss
  end

  def bonus(multiplier)
    salary * multiplier
  end
end
